package dk.itu.bg14.BioBookingGUI;

import dk.itu.bg14.BioBookingGUI.Controller.BioBookingController;
import dk.itu.bg14.BioBookingGUI.Controller.ReservationController;
import dk.itu.bg14.BioBookingGUI.Model.Film;
import dk.itu.bg14.BioBookingGUI.Model.Forestillinger;
import dk.itu.bg14.BioBookingGUI.Model.Reservationer;
import dk.itu.bg14.BioBookingGUI.Model.Sal;
import dk.itu.bg14.BioBookingGUI.Controller.BioBookingController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.sql.*;

public class Main extends Application{
    Stage primaryStage;
    private Connection con;
    private Statement st;
    private ResultSet rs;
    private Statement st2;
    private ResultSet rs2;
    private ObservableList<Forestillinger> forestillingList = FXCollections.observableArrayList();
    private ObservableList<Reservationer> reservationsList = FXCollections.observableArrayList();

    //Connects to the MySQL database and calls the getData() method
    public Main() {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection("jdbc:mysql://mysql.itu.dk/BioBookingSystem", "akr", "awfu0112"); //Connects to our database
            st = con.createStatement();
            st2 = con.createStatement();
            getData();


        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }

    /*Gets all the data from the various tables in the database and stores them in variables
    sql is the information needed to display the showlist in the "show"-window.
    sql2 is the information needed to display the reservationslist in the "delete reservation"-window
    */

    public void getData() throws SQLException {
        try {
            String sql = "SELECT Titel, Dato, Tidspunkt, SalNr, Sæder, Rækker, ForestillingsID " +
                    "FROM Film, Forestillinger, Sal " +
                    "WHERE Film.FilmID = Forestillinger.FilmID AND Sal.SalNr = Forestillinger.Sal ";

            String sql2 = "SELECT KundeNr, RækkeOgSæde, ForestillingsID, SædeReservationsID " +
                    "FROM SædeReservation ";


            rs = st.executeQuery(sql);
            rs2 = st2.executeQuery(sql2);

            /*
            Stores the retrieved data in variables and is then inserted into an observableList
             */
            while(rs2.next()){
                String KundeNr = rs2.getString("KundeNr");
                String RækkeOgSæde = rs2.getString("RækkeOgSæde");
                String forestillingsId = rs2.getString("ForestillingsID");
                int sædeReservationsId = rs2.getInt(("SædeReservationsID"));


                Reservationer reservation = new Reservationer(KundeNr,RækkeOgSæde,forestillingsId,sædeReservationsId);
                reservationsList.add(reservation); //Stores the retrieved data in an obervableList
            }

            /*
            Stores the retrieved data in variables and is then inserted into an observableList
             */
            while (rs.next()) {
                String Titel = rs.getString("Titel");
                String Dato = rs.getString("Dato");
                String Tidspunkt = rs.getString("Tidspunkt");
                int Sæder = rs.getInt("Sæder");
                int Rækker = rs.getInt("Rækker");
                int Sal = rs.getInt("SalNr");
                int ForestillingsID = rs.getInt("ForestillingsID");




                Film film = new Film(Titel);
                Sal sal = new Sal(Sæder, Rækker, Sal);
                Forestillinger forestilling = new Forestillinger(film, sal, Dato, Tidspunkt, ForestillingsID);
                forestillingList.add(forestilling); //Stores the retrieved data in an obervableList

            }

        } finally {
            rs.close();
            st.close();
            rs2.close();
            st2.close();
        }
    }

    public ObservableList<Reservationer> getReservationsList(){return reservationsList;}

    public ObservableList<Forestillinger> getForestillingList(){
        return forestillingList;
    }

    /*
    Starts the primary stage and inserts the first window/stage called BioBooking in it
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        showBioBooking(primaryStage);
    }

    //Draws the biobooking mainwindow, which consists of the two tabs "forestillinger" and "slet reservation"
    public void showBioBooking (Stage primaryStage) {
        try {
            //Load BioBookingGUI
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/BioBooking.fxml"));
            Parent root = loader.load();

            primaryStage.setTitle("BioBooking");
            primaryStage.setScene(new Scene(root));

            primaryStage.show();

            BioBookingController controller = loader.getController();
            controller.setMain(this);

        }catch (Exception e) {
            System.out.println("Error: "+e);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
