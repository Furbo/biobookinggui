package dk.itu.bg14.BioBookingGUI.Controller;

import dk.itu.bg14.BioBookingGUI.Main;
import dk.itu.bg14.BioBookingGUI.Model.Forestillinger;
import dk.itu.bg14.BioBookingGUI.Model.Reservationer;
import dk.itu.bg14.BioBookingGUI.Model.Sal;
import javafx.beans.property.StringProperty;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.sql.*;
import java.util.Collection;

import static java.awt.Color.*;

public class BioBookingController {

        @FXML
        private TableView<Forestillinger> forestillingsTable;
        @FXML
        private TableColumn<Forestillinger, String> filmCol;
        @FXML
        private TableColumn<Forestillinger, String> datoCol;
        @FXML
        private TableColumn<Forestillinger, String> tidspunktCol;
        @FXML
        private TableColumn<Forestillinger, String> salCol;
        @FXML
        private TableView<Reservationer> reservationsTable;
        @FXML
        private TableColumn<Reservationer, String> forestillingsIDCol;
        @FXML
        private TableColumn<Reservationer, String> kundeNrCol;
        @FXML
        private TableColumn<Reservationer, String> rækkeOgSædeCol;
        @FXML
        private TextField filterField;
        @FXML
        private TextField filterField2;
        @FXML
        private Button removeReservation;
        @FXML
        public int getForestillingsID() {
            return forestillingsID;
         }
        @FXML
        private int forestillingsID;
        private Connection con;
        private Statement st;
        private Main main;
        private Stage primaryStage;

        /**
         * The constructor is called before the initialize() method.
         */
        public BioBookingController() {
        }

        /**
         * Initializes the controller class. This method is automatically called
         * after the fxml file has been loaded.
         */
        @FXML
        private void initialize()  {
            filmCol.setCellValueFactory(cellData -> cellData.getValue().titleProperty()); //Adds data from film to the Film Column
            salCol.setCellValueFactory(cellData -> cellData.getValue().salProperty()); //adds data from sal to the Sal column
            datoCol.setCellValueFactory(cellData -> cellData.getValue().datoProperty()); //adds dato data from forestillinger to dato column
            tidspunktCol.setCellValueFactory(cellData -> cellData.getValue().tidspunktProperty()); //adds tidspunkt data from forestillinger to tidspunkts column
            filterField.setPromptText("Søg efter film/dato/tidspunkt"); //information text in textfield that tells the user what to search for

            kundeNrCol.setCellValueFactory(cellData -> cellData.getValue().kundeNrProperty()); //adds kundeNr data from reservationer to kundeNr column
            rækkeOgSædeCol.setCellValueFactory(cellData -> cellData.getValue().rækkeOgSædeProperty()); //adds række og sæde data from reservationer to RækkeOgSæde column
            forestillingsIDCol.setCellValueFactory(cellData -> cellData.getValue().forestillingsIdProperty());//adds forestillingsID data from reservation to forestillings id Column
            filterField2.setPromptText("Søg efter kundeNr");

        }

        /*
        This method is responsible for filtering userinput in the searchbar
         */
        @FXML
        public void handleFiltering() {

            //Wraps the ObservableList in a filteredList
            FilteredList<Forestillinger> filteredData = new FilteredList<>(main.getForestillingList(), p -> true);
            filterField.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(forestillinger -> {
                    if(newValue == null || newValue.isEmpty()){
                        return true;
                    }

                    //compares with filter text
                    String lowerCaseFilter = newValue.toLowerCase();


                    if(forestillinger.getDato().toLowerCase().indexOf(lowerCaseFilter) !=-1){
                        return true;
                    }else if(forestillinger.getTidspunkt().toLowerCase().indexOf(lowerCaseFilter) != -1){
                        return true;
                    }else if(forestillinger.getFilm().getTitel().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                        return true;
                    }
                    return false;
                });
            });

            //Wraps the filtered list in a sorted list
            SortedList<Forestillinger> sortedData = new SortedList<>(filteredData);

            //Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(forestillingsTable.comparatorProperty());

            //add Sorted (and filtered) data to the table
            forestillingsTable.setItems(sortedData);
        }
        @FXML
        public void handleFiltering2() {

            //Wraps the ObservableList in a filteredList
            FilteredList<Reservationer> filteredData = new FilteredList<>(main.getReservationsList(), p -> true);
            filterField2.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(reservationer -> {
                    if(newValue == null || newValue.isEmpty()){
                        return true;
                    }

                    //compares with filter text
                    String lowerCaseFilter = newValue.toLowerCase();


                    if(reservationer.getKundeNr().toLowerCase().indexOf(lowerCaseFilter) !=-1){
                        return true;
                    }else if(reservationer.getRækkeOgSæde().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                        return true;
                    }
                    return false;
                });
            });

            //Wraps the filtered list in a sorted list
            SortedList<Reservationer> sortedData = new SortedList<>(filteredData);

            //Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(reservationsTable.comparatorProperty());

            //add Sorted (and filtered) data to the table
            reservationsTable.setItems(sortedData);
        }


        /*
        Gets the selected Forestilling from the forestillingslist
         */
        @FXML
        public Forestillinger getSelectedF(){
            return forestillingsTable.getSelectionModel().getSelectedItem();
        }

        /*
        Gets the selected reservation from the reservationlist
         */
        @FXML
        public Reservationer getSelectedR(){
            return reservationsTable.getSelectionModel().getSelectedItem();
        }

        /*
        Removes the selected reservation from the database and shows a confirmation dialog when executed
         */
        @FXML
        private void removeReservation(ActionEvent event)throws SQLException{
            Reservationer selectedReservation = getSelectedR();
            if(selectedReservation!=null){ //checks if a reservation has been selected
                connectDB();
                int id = selectedReservation.getSædeReservationsID();
                try{
                    st.executeUpdate("DELETE FROM SædeReservation WHERE " + id + " = SædeReservation.SædeReservationsID");

                }finally{
                    con.close();
                }
                showConfirmationDialog("Reservation er slettet!\nProgrammet skal genstartes før det kan ses på listen!","Reservation slettet!");
            }
        }

        public static void showConfirmationDialog(String infoMessage, String titleBar) {
            JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
        }

        /*
        Creates connection
         */
        public void connectDB()throws SQLException{
            try{
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://mysql.itu.dk/BioBookingSystem", "akr", "awfu0112");
                st = (Statement) con.createStatement();

            }catch (Exception e){
                System.out.println("Error: "+e);
            }
        }

        /*
        Opens the selected show in a separate window which is responsible for seat reservation
         */
        @FXML
        private void openReserver(ActionEvent event) throws SQLException {

            Forestillinger selectedForestilling = getSelectedF();

            if(selectedForestilling!=null) { //checks if a show has been chosen
                Sal s = selectedForestilling.getSal();
                ReservationController bioSal = new ReservationController();
                bioSal.setSal(s); //Sets the theater to be the size of the theater that is chosen for the particular show
                bioSal.showBio(selectedForestilling); //creates the new display of the theater, that matches the chosen show
            }
        }

        /*
        adds the forestillingslist and reservationslist to the forestillingstable and reservationstable
         */
        public void setMain(Main main) throws SQLException {
            this.main = main;
            forestillingsTable.setItems(main.getForestillingList());
            handleFiltering();
            reservationsTable.setItems(main.getReservationsList());
            handleFiltering2();

        }
    }