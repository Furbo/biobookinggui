package dk.itu.bg14.BioBookingGUI.Controller;


import dk.itu.bg14.BioBookingGUI.Main;
import dk.itu.bg14.BioBookingGUI.Model.Forestillinger;
import dk.itu.bg14.BioBookingGUI.Model.Sal;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javax.swing.*;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ReservationController {

    private Sal _sal;
    private Stage reserverStage;
    private ArrayList<String> selectedRowAndSeatNr = new ArrayList<>();
    private ArrayList<String> reservedRowAndSeatNr = new ArrayList<>();
    @FXML
    private Button reserve;
    @FXML
    private GridPane bioSal;
    private Button[][] sæder;
    private Connection con;
    private Statement st;
    private Statement st2;
    private ResultSet rs;
    private ResultSet rs2;
    private String kundeNr;
    private int forestillingsID;
    @FXML
    private Label titleLabel;
    @FXML
    private Label salLabel;
    @FXML
    private Label datoLabel;
    @FXML
    private Label tidspunktLabel;
    @FXML
    private TextField telefonNr;

    public void setSal(Sal sal) {
        _sal = sal;
    }


    /*
    Draws the reservation window, which is specific to the show chosen in the "forestillings"-window
     */
    @FXML
    public void showBio(Forestillinger forestilling) throws SQLException {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Reservation.fxml"));
            Parent root = loader.load();
            forestillingsID = forestilling.getForestillingsID();

            Stage reservationStage = new Stage();
            reservationStage.setTitle("Reservation");
            Scene scene = new Scene(root);
            reservationStage.setScene(scene);

            /*
            loads and set the texts of the dynamic labels, that contains relevant information about the chosen movie
             */
            titleLabel = (Label) loader.getNamespace().get("titleLabel");
            salLabel = (Label) loader.getNamespace().get("salLabel");
            datoLabel = (Label) loader.getNamespace().get("datoLabel");
            tidspunktLabel = (Label) loader.getNamespace().get("tidspunktLabel");
            titleLabel.setText(forestilling.getFilm().getTitel());
            salLabel.setText(forestilling.getSal().getSalNr());
            datoLabel.setText(forestilling.getDato());
            tidspunktLabel.setText(forestilling.getTidspunkt());

            telefonNr = (TextField) loader.getNamespace().get("telefonNr");

            bioSal = (GridPane) loader.getNamespace().get("bioSal");

            Button reserveButton = (Button) loader.getNamespace().get("reserve");

            /*
            When the reserve button is clicked, it checks if the userinput is an eight digit phone number,
            and if it is, it inserts the chosen reservations in the database
             */
            reserveButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (isInputValid()) { //checks if the inserted phonenumber is valid
                        kundeNr = telefonNr.getText();
                        System.out.print(kundeNr);
                        reservationStage.close();
                        showConfirmationDialog("Følgende sæder er reserveret: " + selectedRowAndSeatNr + "", "Succes"); //confirmation dialog that tells the user that a reservation has been made sucesfully
                            for (String rækkeOgSædeNr : selectedRowAndSeatNr) {
                            connectDB(rækkeOgSædeNr, kundeNr);
                        }
                    }
                }
            });

            sæder = new Button[_sal.getRækker()][_sal.getSæder()];

            /*
            Gets information from the database about which seats are reserved for the particular show, and then draws
            the theater. Green seats are drawn if they are not reserved, and red if they are reserved
             */
            reservedData();
            for (int i = 0; i < sæder.length; i++) {
                for (int j = 0; j < sæder[i].length; j++) {
                    sæder[i][j] = new Button();
                    sæder[i][j].setPrefSize(25, 25); //set the sizes of the chairs
                    sæder[i][j].setStyle("-fx-background-color: Green;"); //draws the seats green
                    sæder[i][j].setId("Række: " + (i + 1) + " Sæde: " + (j + 1)); //sets a string id to be their place in the 2d array plus 1
                    String id = sæder[i][j].getId();
                    for (String s : reservedRowAndSeatNr) { //For every reserved seat in this theater the if statement runs
                        if (s.equals(id)) { //checks if the seat in the 2d array is equal to a seat stored in the reservedRowAndSeatNr arrayList
                            sæder[i][j].setStyle("-fx-background-color: Red;"); //if it is equal IE it is already reserved, it paints it red!
                        }
                    }
                    bioSal.add(sæder[i][j], j, i + 2); //adds the colored and sized seats to the theater
                }
            }
            selectSeats();
            reservationStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    when a green seat is clicked, its position is added to a temporary arraylist, and the button is painted yellow
     */
    public void selectSeats() {
        for (int i = 0; i < sæder.length; i++) {
            for (int j = 0; j < sæder[i].length; j++) {

                sæder[i][j].setOnMouseClicked(new EventHandler<MouseEvent>() { //eventhandler: the following happens when a seat is clicked
                    @Override
                    public void handle(MouseEvent arg0) {
                        Button b = (Button) arg0.getSource();
                        String buttonID = "" + b.getId() + ""; //get the id of the button clicked

                        if (b.getStyle() == "-fx-background-color: Green;") { //if the seat is green IE not reserved
                            b.setStyle("-fx-background-color: Yellow;");  //paint it yellow
                            selectedRowAndSeatNr.add(buttonID); //store in a temporary arraylist which contains seats to be reserved

                        } else if (b.getStyle() == "-fx-background-color: Yellow;") { //if yellow IE to-be reserved and it is then clicked
                            b.setStyle("-fx-background-color: Green;"); //paint it green again
                            selectedRowAndSeatNr.remove(buttonID); //and remove it from the temporary arraylist
                        }
                    }
                });
            }
        }
    }

    public static void showConfirmationDialog(String infoMessage, String titleBar) {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    //Check if input is a phonenumber
    public boolean isInputValid() {
        String errorMessage = "";
        if (telefonNr.getText() == null || telefonNr.getText().length() == 0) {
            errorMessage += "Ugyldigt telefonNr - prøv igen!\n";
        }
        // try to parse the postal code into an int.
        try {
            Integer.parseInt(telefonNr.getText());
        } catch (NumberFormatException e) {
            errorMessage += "Ugyldigt telefonNr - prøv igen!\n";
        }
        if (telefonNr.getText().length() != 8) {
            errorMessage += "Ugyldigt telefonNr - prøv igen!\n";
        }
        if (errorMessage.length() == 0) {
            return true;
        } else {
            System.out.println("Ugyldigt TelefonNR - prøv igen!");
            return false;
        }
    }

    /*
    Attempt to connect to the database
     */
    public void connectDB(String buttonID, String kundeNr) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://mysql.itu.dk/BioBookingSystem", "akr", "awfu0112");
            st = (Statement) con.createStatement();
            insertData(buttonID, kundeNr);

        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }

    /*
    Inserts the reservation into the database
     */
    public void insertData(String buttonID, String kundeNr) throws SQLException {
        try {
            st.executeUpdate("INSERT INTO SædeReservation (ForestillingsID, KundeNr, RækkeOgSæde) VALUES (" +
                    "" + forestillingsID + "," + kundeNr + " , " + "'" + buttonID + "'" + ")");
        } finally {
            con.close();
        }
    }

    /*
    Asks the database which seats are reserved and stores the result in an arraylist
     */
    public void reservedData() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://mysql.itu.dk/BioBookingSystem", "akr", "awfu0112");
            st2 = (Statement) con.createStatement();
            rs2 = st2.executeQuery("SELECT RækkeOgSæde FROM SædeReservation WHERE SædeReservation.ForestillingsID = " + forestillingsID + "");
            while (rs2.next()) {
                String RoS = rs2.getString("RækkeOgSæde");
                reservedRowAndSeatNr.add(RoS);
            }
        } catch (Exception e) {
            System.out.print("Error: " + e);

        }
    }
}