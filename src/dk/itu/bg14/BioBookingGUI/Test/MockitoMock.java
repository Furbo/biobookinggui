package dk.itu.bg14.BioBookingGUI.Test;

import dk.itu.bg14.BioBookingGUI.Model.Reservationer;
import org.junit.Assert;
import org.junit.Test;
import dk.itu.bg14.BioBookingGUI.Test.DatabaseManager;
import dk.itu.bg14.BioBookingGUI.Model.Film;
import dk.itu.bg14.BioBookingGUI.Model.Forestillinger;
import dk.itu.bg14.BioBookingGUI.Model.Sal;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class MockitoMock {
    public MockitoMock() {
    }


    @Test
    public void getAllForestillinger() {
        try {
            ResultSet rs = (ResultSet) Mockito.mock(ResultSet.class);

            //Tests if the if rs.getString() and rs.getInt() return prober values
            Mockito.when(rs.getString("Titel")).thenReturn("Interstellar", "Dum og Dummere 2", "Dommeren");
            Mockito.when(rs.getString("Dato")).thenReturn("2014-12-01", "2014-12-02", "2014-12-02");
            Mockito.when(rs.getString("Tidspunkt")).thenReturn("12:00:00", "18:00:00", "20:00:00");
            Mockito.when(rs.getInt("Rækker")).thenReturn(9, 7, 5);
            Mockito.when(rs.getInt("Sæder")).thenReturn(10, 8, 6);
            Mockito.when(rs.getInt("SalNr")).thenReturn(1, 2, 3);
            Mockito.when(rs.getInt("ForestillingsID")).thenReturn(10, 1, 84);

            //Tests if it stops rs.next()
            Mockito.when(rs.next()).thenReturn(true, true, true , false);

            //Tests if the executeQuary statement returns a resultset
            Statement mockStatement = (Statement) Mockito.mock(Statement.class);
            Mockito.when(mockStatement.executeQuery("SELECT Titel, Dato, Tidspunkt, SalNr, Sæder, Rækker, ForestillingsID " +
                    "FROM Film, Forestillinger, Sal " +
                    "WHERE Film.FilmID = Forestillinger.FilmID AND Sal.SalNr = Forestillinger.Sal ")).thenReturn(rs);

            //Tests if the connection returns a statement
            Connection mockConnection = (Connection) Mockito.mock(Connection.class);
            Mockito.when(mockConnection.createStatement()).thenReturn(mockStatement);

            //Tests if the given Array is made proberly
            Film film1 = new Film("Interstellar");
            Film film2 = new Film("Dum og Dummere 2");
            Film film3 = new Film("Dommeren");

            Sal sal1 = new Sal(10, 9, 1);
            Sal sal2 = new Sal(8, 7, 2);
            Sal sal3 = new Sal(6, 5, 3);

            DatabaseManager mgr = new DatabaseManager(mockConnection);
            List forestillinger = mgr.getAllForestillinger();
            ArrayList<Forestillinger> expectedForestillinger = new ArrayList<>();

            expectedForestillinger.add(new Forestillinger(film1, sal1,"2014-12-01", "12:00:00", 10));
            expectedForestillinger.add(new Forestillinger(film2, sal2, "2014-12-02", "18:00:00", 1));
            expectedForestillinger.add(new Forestillinger(film3, sal3,"2014-12-02", "20:00:00",  84));


            Assert.assertArrayEquals(expectedForestillinger.toArray(), forestillinger.toArray());


        } catch (SQLException e) {
            ;
        }
    }
        @Test
        public void getAllReservations() {
            try {
                ResultSet rs = (ResultSet) Mockito.mock(ResultSet.class);

                //Tests if the if rs.getString() and rs.getInt() return prober values
                Mockito.when(rs.getString("KundeNr")).thenReturn("1", "5", "10");
                Mockito.when(rs.getString("RækkeOgSæde")).thenReturn("Række 7 Sæde 7", "Række 7 Sæde 6", "Række 7 Sæde 5");
                Mockito.when(rs.getString("ForestillingsID")).thenReturn("1", "10", "84");
                Mockito.when(rs.getInt("SædeReservationsID")).thenReturn(9, 7, 5);

                //Tests if it stops rs.next()
                Mockito.when(rs.next()).thenReturn(true, true, true , false);

                //Tests if the executeQuary statement returns a resultset
                Statement mockStatement = (Statement) Mockito.mock(Statement.class);
                Mockito.when(mockStatement.executeQuery("SELECT KundeNr, RækkeOgSæde, ForestillingsID, SædeReservationsID " +
                        "FROM SædeReservation ")).thenReturn(rs);

                //Tests if the connection returns a statement
                Connection mockConnection = (Connection) Mockito.mock(Connection.class);
                Mockito.when(mockConnection.createStatement()).thenReturn(mockStatement);

                //Tests if the given Array is made proberly

                DatabaseManager mgr = new DatabaseManager(mockConnection);
                List reservationer = mgr.getAllReservationer();
                ArrayList<Reservationer> expectedReservationer = new ArrayList<>();

                expectedReservationer.add(new Reservationer("1", "Række 7 Sæde 7", "1",  9));
                expectedReservationer.add(new Reservationer("5", "Række 7 Sæde 6", "10",  7));
                expectedReservationer.add(new Reservationer("10", "Række 7 Sæde 5", "84",  5));

                Assert.assertArrayEquals(expectedReservationer.toArray(), reservationer.toArray());

            } catch (SQLException e) {
                ;
            }
        }
}
