package dk.itu.bg14.BioBookingGUI.Test;

import dk.itu.bg14.BioBookingGUI.Model.Reservationer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReservationerTest {
    private Reservationer reservation1;
    private Reservationer reservation2;
    private Reservationer reservation3;

    /*
    Objekter af Reservationer med dummydata oprettes før hver test
     */
    @Before
    public void beforeTests(){
        reservation1 = new Reservationer("12345678","Række 5 Sæde 4","1",1);
        reservation2 = new Reservationer("22446688","Række 4 Sæde 5","2",2);
        reservation3 = new Reservationer("12345678","Række 5 Sæde 4","1",1);
    }

    /*
    Her testes for om et objekt af Reservationer faktisk returner det korrekte med sine get-metoder
     */
    @Test
    public void returnReservationerTest(){
        Assert.assertEquals("12345678",reservation1.getKundeNr()); //tester om en reservations getKundeNr returnerer det korrekte kundeNr
        Assert.assertEquals("Række 5 Sæde 4",reservation1.getRækkeOgSæde()); //tester om en reservations getRækkeOgSæde returnerer den korrekt String af række og sæde
        Assert.assertEquals("1",reservation1.getForestillingsId()); //tester om en reservations getForestillingsId returnerer det korrekte forestillingsId
        Assert.assertEquals(1,reservation1.getSædeReservationsID()); //tester om en reservations getSædeReservationsID returnerer det korrekt SædeReservationsID
    }

    /*
    Tester for om to objekter af Reservationer med forskelligt indhold returnerer noget forskelligt
    Tester også for om to objekter af Reservationer med samme indhold returnerer det samme
     */
    @Test
    public void compareReservationer(){
        Assert.assertNotEquals(reservation1,reservation2); //tester om de to reservationer returnerer noget forskelligt
        Assert.assertEquals(reservation1,reservation3); //tester om to reservationer returnerer det samme
    }


}
