package dk.itu.bg14.BioBookingGUI.Test;


import dk.itu.bg14.BioBookingGUI.Model.Film;
import dk.itu.bg14.BioBookingGUI.Model.Forestillinger;
import dk.itu.bg14.BioBookingGUI.Model.Reservationer;
import dk.itu.bg14.BioBookingGUI.Model.Sal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {
    private Connection con;

    public DatabaseManager(Connection con) {
    this.con = con;
    }

    public List<Forestillinger> getAllForestillinger() {
        ArrayList forestillingList;
        try {
            forestillingList = new ArrayList();
            Statement st = this.con.createStatement();
            ResultSet rs = st.executeQuery("SELECT Titel, Dato, Tidspunkt, SalNr, Sæder, Rækker, ForestillingsID " +
                    "FROM Film, Forestillinger, Sal " +
                    "WHERE Film.FilmID = Forestillinger.FilmID AND Sal.SalNr = Forestillinger.Sal ");

            while (rs.next()) {
                String Titel = rs.getString("Titel");
                String Dato = rs.getString("Dato");
                String Tidspunkt = rs.getString("Tidspunkt");
                int Sæder = rs.getInt("Sæder");
                int Rækker = rs.getInt("Rækker");
                int Sal = rs.getInt("SalNr");
                int ForestillingsID = rs.getInt("ForestillingsID");


                Film film = new Film(Titel);
                dk.itu.bg14.BioBookingGUI.Model.Sal sal = new Sal(Sæder, Rækker, Sal);
                Forestillinger forestilling = new Forestillinger(film, sal, Dato, Tidspunkt, ForestillingsID);
                forestillingList.add(forestilling);

            }
        } catch (SQLException e) {
            forestillingList = null;
        }
        return forestillingList;
    }


    public List<Reservationer> getAllReservationer() {
        ArrayList reservationList;
        try {
            reservationList = new ArrayList();
            Statement st = this.con.createStatement();

            ResultSet rs = st.executeQuery("SELECT KundeNr, RækkeOgSæde, ForestillingsID, SædeReservationsID " +
                "FROM SædeReservation ");

        while(rs.next()){
            String KundeNr = rs.getString("KundeNr");
            String RækkeOgSæde = rs.getString("RækkeOgSæde");
            String forestillingsId = rs.getString("ForestillingsID");
            int sædeReservationsId = rs.getInt("SædeReservationsID");


            Reservationer reservation = new Reservationer(KundeNr,RækkeOgSæde,forestillingsId,sædeReservationsId);

            reservationList.add(reservation);

        }
        } catch (SQLException e) {
            reservationList = null;
        }
        return reservationList;
    }
}
