package dk.itu.bg14.BioBookingGUI.Test;

import dk.itu.bg14.BioBookingGUI.Model.Film;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class FilmTest {
    private Film film1;
    private Film film2;
    private Film film3;

    /*
    Før en test kører kaldes denne metode, som opretter følgende objekter som indeholder noget dummydata
     */
    @Before
    public void beforeTests(){
        film1 = new Film("Interstellar");
        film2 = new Film("Dommeren");
        film3 = new Film("Interstellar");
    }

    /*
    Her tester vi for om get-metoden i Film klassen faktisk returnerer det korrekte
     */
    @Test
    public void returnMovieTest(){
        Assert.assertEquals("Interstellar", film1.getTitel());
    }

    /*
    Her tester vi for om to objekter af Film med forskelligt indhold faktisk returnerer noget forskelligt
    og samtidig om to objekter med samme indhold returnerer det samme
     */
    @Test
    public void compareMovie(){
        Assert.assertNotEquals(film1,film2); //tester om to film returnerer noget foreskelligt
        Assert.assertEquals(film1,film3); //tester om de to film returnerer det samme
    }
}
