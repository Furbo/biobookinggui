package dk.itu.bg14.BioBookingGUI.Test;


import dk.itu.bg14.BioBookingGUI.Model.Film;
import dk.itu.bg14.BioBookingGUI.Model.Forestillinger;
import dk.itu.bg14.BioBookingGUI.Model.Sal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ForestillingerTest {
    private Forestillinger forestilling1;
    private Forestillinger forestilling2;
    private Forestillinger forestilling3;
    private Sal s1 = new Sal(7,7,3); //opretter sal objekter som kan indsættes i forestillingsobjekter
    private Sal s2 = new Sal(5,5,2);
    private Film f1 = new Film("Interstellar"); //opretter film objekter som kan indsættes i forestillingsobjekter
    private Film f2 = new Film("Dommeren");

    /*
    Objekter af Forestillinger med dummydata oprettes
     */
    @Before
    public void beforeTests(){
        forestilling1 = new Forestillinger(f1,s1,"2014-12-01","12:00:00",3);
        forestilling2 = new Forestillinger(f2,s2,"2014-12-02","13:00:00",10);
        forestilling3 = new Forestillinger(f1,s1,"2014-12-01","12:00:00",3);
    }

    /*
    Her testes om et objekt af Forestillinger returnerer det korrekt med sine get-metoder
     */
    @Test
    public void returnForestillingerTest(){
        Assert.assertEquals(f1,forestilling1.getFilm()); //tester om en forestillings getFilm metode returnerer den korrekte film
        Assert.assertEquals(s1,forestilling1.getSal()); //tester om en forestillings getSal metode returnerer den korekte sal
        Assert.assertEquals("2014-12-01",forestilling1.getDato()); //tester om en forestillings getDato metode returnerer den korrekte dato
        Assert.assertEquals("12:00:00",forestilling1.getTidspunkt()); //tester om en forestillings getTidspunkts metode returnerer det korrekte tidspunkt
        Assert.assertEquals(3,forestilling1.getForestillingsID()); //tester om en forestillings getForestillingsID metode returnerer det korrekt forestillingsID
    }

    /*
    tester om to objekter af Forestillinger med samme indhold returnerer det samme,
    tester også om to objekter af Forestillinger med foreskelligt indhold ikke returnerer det samme
     */
    @Test
    public void compareForestillinger(){
        Assert.assertEquals(forestilling1,forestilling3); //tester om de to forestillinger returnerer det samme
        Assert.assertNotEquals(forestilling1,forestilling2); //tester om de to forestillinger ikke returnerer det samme
    }

}
