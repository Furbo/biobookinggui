package dk.itu.bg14.BioBookingGUI.Test;


import dk.itu.bg14.BioBookingGUI.Model.Sal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SalTest {
    private Sal sal1;
    private Sal sal2;
    private Sal sal3;

    /*
    Objekter af Sal med dummydata oprettes før hver test
     */
    @Before
    public void beforeTests(){
        sal1 = new Sal(7,7,3);
        sal2 = new Sal(6,6,2);
        sal3 = new Sal(7,7,3);
    }

    /*
    Tester om et objekt af Sal faktisk returnerer det korrekt med sine get-metoder
     */
    @Test
    public void returnSalTest(){
        Assert.assertEquals(7,sal1.getRækker()); //tester om Sals getRækker returnerer det korrekte antal rækker
        Assert.assertEquals(7,sal1.getSæder()); //tester om en Sals getSæder returnerer det korrekt antal sæder
        Assert.assertEquals("3",sal1.getSalNr()); //tester om en Sals getSalNr returnerer det korrekt salnr
    }


    /*
    Tester for om to objekter af Sal med samme indhold returnerer det samme
    Tester også for om to objekter af Sal med forskelligt indhold returnerer noget forskelligt
     */
    @Test
    public void compareSals(){
        Assert.assertEquals(sal1,sal3); //tester om de to sale returnerer det samme
        Assert.assertNotEquals(sal1,sal2); //tester om de to sale returnerer noget forskelligt
    }
}
