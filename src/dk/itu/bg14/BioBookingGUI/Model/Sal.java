package dk.itu.bg14.BioBookingGUI.Model;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
Sal klassen tager imod 3 parametre
Indeholder Getter og Setter metoder til hver parameter

 */
public class Sal {
    private int rækker;
    private int sæder;
    private int sal;


    public Sal(int rækker, int sæder, int sal) {
        this.rækker = rækker;
        this.sæder = sæder;
        this.sal = sal;
    }

    public int getRækker() {
        return rækker;
    }

    public void setRækker(int rækker) {
        this.rækker = rækker;
    }

    public int getSæder() {
        return sæder;
    }


    public void setSæder(int sæder) {
        this.sæder = sæder;
    }

    public String getSalNr() {
        return "" + sal;
    }

    public void setSal(int sal) {
        this.sal = sal;
    }

    public StringProperty salProperty() {
        String s = "" + sal;
        return new SimpleStringProperty(s);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sal sal1 = (Sal) o;

        if (rækker != sal1.rækker) return false;
        if (sal != sal1.sal) return false;
        if (sæder != sal1.sæder) return false;

        return true;
    }
}

