package dk.itu.bg14.BioBookingGUI.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
Film klassen tager en enkel parameter
Klassen indeholder en Getter og Setter metode til denne parameter.
 */
public class Film  {
    private StringProperty Titel;

    public Film(String Titel) {
        this.Titel = new SimpleStringProperty(Titel);

    }

    public String getTitel() {
        return Titel.get();
    }

    public void setTitel(String Titel) {
        this.Titel.set(Titel);
    }

    public StringProperty titelProperty() {
        return Titel;
    }

    /*
    Overrides javas equals method
    Instead of checking if the two objects are the same object, it checks if two objects contains the same parameter values
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Film film = (Film) o;

        if (Titel != null ? !Titel.getValue().equals(film.Titel.getValue()) : film.Titel.getValue() != null) return false;

        return true;
    }
}

