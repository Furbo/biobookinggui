package dk.itu.bg14.BioBookingGUI.Model;

import javafx.beans.property.*;

/*
Forestillings klassen tager 5 parametre .
Indeholder Getter og Setter metoder til hver parameter
 */
public class Forestillinger {

    private final  Sal sal;
    private final Film film;
    private final String dato;
    private final String tidspunkt;
    private int forestillingsID;



    public Forestillinger(Film film,Sal sal, String dato, String tidspunkt, int forestillingsID) {
        this.sal = sal;
        this.film = film;
        this.dato = dato;
        this.tidspunkt = tidspunkt;
        this.forestillingsID = forestillingsID;

    }

    public Sal getSal() {
        return sal;
    }

    public StringProperty salProperty(){
        return sal.salProperty();
    }

    public Film getFilm() {
        return film;
    }

    public int getForestillingsID() {
        return forestillingsID;
    }

    public StringProperty titleProperty() {
        return film.titelProperty();
    }

    public String getDato() {
        return dato;
    }

    public StringProperty datoProperty() {
        return new SimpleStringProperty(dato);
    }

    public String getTidspunkt() {
        return tidspunkt;
    }

    public StringProperty tidspunktProperty(){

        return new SimpleStringProperty(tidspunkt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Forestillinger that = (Forestillinger) o;

        if (forestillingsID != that.forestillingsID) return false;
        if (dato != null ? !dato.equals(that.dato) : that.dato != null) return false;
        if (film != null ? !film.equals(that.film) : that.film != null) return false;
        if (sal != null ? !sal.equals(that.sal) : that.sal != null) return false;
        if (tidspunkt != null ? !tidspunkt.equals(that.tidspunkt) : that.tidspunkt != null) return false;
        return true;
    }
}


