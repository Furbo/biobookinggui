package dk.itu.bg14.BioBookingGUI.Model;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
Reservations klassen indeholder 4 parametre
Indeholder Getter og Setter metoder til hver parameter
 */
public class Reservationer {

    private final String kundeNr;
    private final String rækkeOgSæde;
    private final String forestillingsId;
    private final int sædeReservationsID;

    public Reservationer (String kundeNr, String rækkeOgSæde, String forestillingsId, int sædeReservationsID){
        this.kundeNr = kundeNr;
        this.rækkeOgSæde = rækkeOgSæde;
        this.forestillingsId = forestillingsId;
        this.sædeReservationsID = sædeReservationsID;
    }

    public String getKundeNr() {
        return kundeNr;
    }
    public StringProperty kundeNrProperty(){
        return new SimpleStringProperty(kundeNr);
    }

    public String getRækkeOgSæde() {
        return rækkeOgSæde;
    }

    public StringProperty rækkeOgSædeProperty(){
        return new SimpleStringProperty(rækkeOgSæde);
    }

    public String getForestillingsId(){
        return forestillingsId;
    }

    public StringProperty forestillingsIdProperty(){
        return new SimpleStringProperty(forestillingsId);
    }

    public int getSædeReservationsID() {
        return sædeReservationsID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reservationer that = (Reservationer) o;

        if (sædeReservationsID != that.sædeReservationsID) return false;
        if (forestillingsId != null ? !forestillingsId.equals(that.forestillingsId) : that.forestillingsId != null)
            return false;
        if (kundeNr != null ? !kundeNr.equals(that.kundeNr) : that.kundeNr != null) return false;
        if (rækkeOgSæde != null ? !rækkeOgSæde.equals(that.rækkeOgSæde) : that.rækkeOgSæde != null) return false;

        return true;
    }
}
